#!/bin/bash

privileged_cmds(){
    ## Allow passwordless privilege escalation
    if [[ -f /etc/sudoers ]]; then
        sed -i 's|^%sudo\s\+ALL=(ALL:ALL) ALL|%sudo ALL=\(ALL:ALL\) NOPASSWD:ALL|g' /etc/sudoers
    fi

    ## Install Ansible
    apt-get -y update
    apt-get -y install software-properties-common
    apt-add-repository ppa:ansible/ansible
    apt-get -y update
    apt-get -y install ansible git
}

# if [[ "$(whoami)" == "root" ]]; then
#     privileged_cmds > /dev/null 2>&1
# else
#     FUNC=$(declare -f privileged_cmds)
#     sudo bash -c "$FUNC; privileged_cmds"
# fi

## Run the fresh_start.yml playbook
run_playbook(){
    cd ansible || return "$?"
    # ansible-galaxy install -p roles -r security-reqs.yml
    # mv roles/Ubuntu1804-CIS roles/0-hardening
    # git clone https://github.com/konstruktoid/ansible-role-hardening.git roles/1.hardening
    ansible-playbook -i inventory/hosts.ini fresh_start.yml
}

run_playbook
