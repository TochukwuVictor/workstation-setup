FROM ubuntu:eoan as eoan-ansible

RUN apt update -y && \
    apt install -y sudo

RUN useradd -ms /bin/bash testuser && \
    usermod -a -G sudo testuser
RUN sed -i \
    's|^%sudo\s\+ALL=(ALL:ALL) ALL|%sudo ALL=\(ALL:ALL\) NOPASSWD:ALL|g' \
    /etc/sudoers

USER testuser
WORKDIR /home/testuser

COPY deploy.sh deploy.sh

RUN sed -i 's/run_playbook$/#run_playbook/g' deploy.sh
RUN ./deploy.sh
