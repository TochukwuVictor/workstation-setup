#!/bin/bash

EMACS_FILE_BK_DIR="$1"

dir_last_char="$(echo "${EMACS_FILE_BK_DIR: -1}")"
if ! [[ "$dir_last_char" == "/" ]]; then
    EMACS_FILE_BK_DIR="$EMACS_FILE_BK_DIR/"
fi

find / -type f -name "*~" -not -path "/var/log/journal*" 2>&1 | \
    while read line; do
        if ! [[ "$line" == *"Permission denied"* ]]; then
            mv "$line" "$EMACS_FILE_BK_DIR"
        fi
    done
