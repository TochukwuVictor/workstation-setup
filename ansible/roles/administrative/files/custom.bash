################################################################################
# History Settings
################################################################################

# Eternal bash history.
# ---------------------
# Undocumented feature which sets the size to "unlimited".
# http://stackoverflow.com/questions/9457233/unlimited-bash-history
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="[%F %T] "
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=~/.bash_eternal_history
# Force prompt to write history after every command.
# http://superuser.com/questions/20900/bash-history-loss
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoredups

# append to the history file, don't overwrite it
shopt -s histappend


################################################################################
# Functions
################################################################################


cr=`echo $'\n.'`
cr=${cr%.}
cr_with_arrow=`echo $'\n> '`
cr_with_arrow=${cr_with_arrow%.}


add_lambda_dirs_to_python_path() {
  lambda_function_dir="$(pwd)/lambda"
  if [[ -d "$lambda_function_dir" ]]; then
    echo "Adding $lambda_function_dir to PYTHONPATH..."
    PYTHONPATH="$lambda_function_dir:$PYTHONPATH"
    export PYTHONPATH
    echo "Current PYTHONPATH - $PYTHONPATH"
    echo
  fi
}

add_services_lambda_dirs_to_python_path() {
  services_dir="$(pwd)/services"
  if [[ -d "$services_dir" ]]; then
    cd "$services_dir" || return $?
    ls -tr | while read dir; do
      echo "Service directory found: $dir"
      cd "$dir" || return $?
      echo "Checking for lambda functions..."
      echo
      add_lambda_dirs_to_python_path
      cd ..
    done
    cd ..
    echo "Current PYTHONPATH - $PYTHONPATH"
  fi
}

export-itg-playground() {
    mfa_code="$1"
    profile_name="itg-play"
    token_reponse="$(aws sts get-session-token \
        --serial-number arn:aws:iam::439458311129:mfa/victort \
        --token-code "$mfa_code" \
        --profile core )"
    export AWS_ACCESS_KEY_ID="$(echo "$token_reponse" | jq -r .Credentials.AccessKeyId)"
    export AWS_SECRET_ACCESS_KEY="$(echo "$token_reponse" | jq -r .Credentials.SecretAccessKey)"
    export AWS_SESSION_TOKEN="$(echo "$token_reponse" | jq -r .Credentials.SessionToken)"

    assume_role_response="$(aws sts assume-role \
        --role-arn arn:aws:iam::260621117368:role/SubCloudAdmin \
        --role-session-name $profile_name)"

    unset AWS_ACCESS_KEY_ID
    unset AWS_SECRET_ACCESS_KEY
    unset AWS_SESSION_TOKEN

    AWS_ACCESS_KEY_ID="$(echo "$assume_role_response" | jq -r .Credentials.AccessKeyId)"
    AWS_SECRET_ACCESS_KEY="$(echo "$assume_role_response" | jq -r .Credentials.SecretAccessKey)"
    AWS_SESSION_TOKEN="$(echo "$assume_role_response" | jq -r .Credentials.SessionToken)"

    sed -i "/$profile_name/,+3 d" ~/.aws/credentials

    echo "[$profile_name]" >> ~/.aws/credentials
    echo "aws_access_key_id=$AWS_ACCESS_KEY_ID" >> ~/.aws/credentials
    echo "aws_secret_access_key=$AWS_SECRET_ACCESS_KEY" >> ~/.aws/credentials
    echo "aws_session_token=$AWS_SESSION_TOKEN" >> ~/.aws/credentials
    export AWS_PROFILE="$profile_name"
}

export-itg-easi() {
    mfa_code="$1"
    profile_name="itg-easi"
    token_reponse="$(aws sts get-session-token \
        --serial-number arn:aws:iam::439458311129:mfa/victort \
        --token-code "$mfa_code" \
        --profile core )"
    export AWS_ACCESS_KEY_ID="$(echo "$token_reponse" | jq -r .Credentials.AccessKeyId)"
    export AWS_SECRET_ACCESS_KEY="$(echo "$token_reponse" | jq -r .Credentials.SecretAccessKey)"
    export AWS_SESSION_TOKEN="$(echo "$token_reponse" | jq -r .Credentials.SessionToken)"

    assume_role_response="$(aws sts assume-role \
        --role-arn arn:aws:iam::935631159365:role/SubITOPS \
        --role-session-name $profile_name)"

    unset AWS_ACCESS_KEY_ID
    unset AWS_SECRET_ACCESS_KEY
    unset AWS_SESSION_TOKEN

    AWS_ACCESS_KEY_ID="$(echo "$assume_role_response" | jq -r .Credentials.AccessKeyId)"
    AWS_SECRET_ACCESS_KEY="$(echo "$assume_role_response" | jq -r .Credentials.SecretAccessKey)"
    AWS_SESSION_TOKEN="$(echo "$assume_role_response" | jq -r .Credentials.SessionToken)"

    sed -i "/$profile_name/,+3 d" ~/.aws/credentials

    echo "[$profile_name]" >> ~/.aws/credentials
    echo "aws_access_key_id=$AWS_ACCESS_KEY_ID" >> ~/.aws/credentials
    echo "aws_secret_access_key=$AWS_SECRET_ACCESS_KEY" >> ~/.aws/credentials
    echo "aws_session_token=$AWS_SESSION_TOKEN" >> ~/.aws/credentials
    export AWS_PROFILE="$profile_name"
}


################################################################################
# Startup Configs
################################################################################

echo "========================================================================="
echo "Don't forget to:"
echo "========================================================================="
echo

echo "========================================================================="
echo "Setting environment variables"
echo "========================================================================="
echo

export PATH="$HOME/.local/bin:/usr/local/bin:$PATH"
echo "Current PATH: $PATH"

# echo "******************************************************"
# echo "Adjusting system settings "
# echo "******************************************************"
# echo

# export DISPLAY="$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0"
# export BROWSER=/usr/bin/firefox
# sudo sysctl -p


# add_lambda_dirs_to_python_path
# add_services_lambda_dirs_to_python_path

