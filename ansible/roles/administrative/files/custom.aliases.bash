###GENERAL OS COMMANDS###

#~~AWSCLI~~#
alias modec2='aws ec2 modify-instance-attribute --instance-id'

#~~Control RunLevel~~#
alias brb='shutdown -r now'
alias bye='shutdown -h now'

#~~Environment Vars~~#
alias aws_linux_filters='export LINUX_FILTER_KEY="Platform" ; export LINUX_FILTER_VALUES="Linux"'
alias pmc_ssm_param_vars='export API_KEY_ID_SSM_PARAM_NAME=/pmc/api-key-id \
export API_PRIVATE_KEY_SSM_PARAM_NAME=/pmc/api-private-key \
export PMC_V1_URL=https://console.parkmycloud.com \
export PMC_V2_URL=https://console.parkmycloud.com/v2'
alias pmc_creds='export PMC_USERNAME=$(aws --profile ncmms ssm get-parameter --name /pmc/username | jq -r ".Parameter.Value") \
export PMC_PASSWORD=$(aws --profile ncmms ssm get-parameter --name /pmc/password --with-decryption | jq -r ".Parameter.Value") \
export PMC_APPID=$(aws --profile ncmms ssm get-parameter --name /pmc/appid --with-decryption | jq -r ".Parameter.Value") \
export PMC_API_PRIVATE_KEY=$(aws --profile ncmms ssm get-parameter --name /pmc/api-private-key --with-decryption | jq -r ".Parameter.Value") \
export PMC_API_KEY_ID=$(aws --profile ncmms ssm get-parameter --name /pmc/api-key-id --with-decryption | jq -r ".Parameter.Value")'
alias nessus_creds='export NESSUS_ACCESS_KEY=$(aws --profile ncmms ssm get-parameter --name /nessus/api/access_key --with-decryption | jq -r ".Parameter.Value") \
export NESSUS_SECRET_KEY=$(aws --profile ncmms ssm get-parameter --name /nessus/api/secret_key --with-decryption | jq -r ".Parameter.Value")'
alias autopatch_google_api_creds='export GCP_CREDS_SSM_PARAM_NAME="/google/api/auto-patch-svc-acc"'
alias nessus_reporter_google_api_creds='export GCP_CREDS_SSM_PARAM_NAME="/google/api/nessus-reporter-svc-acc"'

#~~Source~~#
alias srcrc='source ${HOME}/.bashrc'
alias srcprf='source ${HOME}/.bash_profile'
alias srcal='source ${HOME}/.bashrc'

#~~Location~~#
alias updatedb='sudo updatedb'
alias loc='locate'
alias grp='grep -nrw'

#~~Logs~~#
alias followmsgs='follow /var/log/messages'
alias followaud='follow /var/log/audit/audit.log'
alias followsec='follow /var/log/secure'
alias showmsmgs='show /var/log/messages'
alias showaud='show /var/log/audit/audit.log'
alias showsec='show /var/log/secure'

#~~Manipulation~~#
alias cpr='cp -r'
alias rmd='rm -r'

#~~Navigation~~#
alias cdbooks='cd ${HOME}/Dropbox/Scripts/cookbooks'
alias cdmax='cd ${HOME}/Dropbox/Scripts/cookbooks/maximo'
alias cdjavaupdate='cd ${HOME}/Dropbox/Scripts/cookbooks/itg-java-update'
alias ..='cd ..'
alias cdscripts='cd ${HOME}/Dropbox/Scripts/'
alias cdautopatch="cd ${HOME}/Dropbox/Scripts/projects/serverless-automated-patching/"

#~~Permissions~~#
alias chm='chmod -R'
alias cho='chown -R'

#~~Transfers~~#
alias rsync='rsync -avzP'
alias scpr='scp -r'
alias scp='cd ${HOME}/Filetransfer && scp'
alias win_screenshot_pull='mv /mnt/c/Users/tjvic/Artifacts/Screenshots/Transfer/* .'

#~~View Info~~#
alias aliases='cat ${HOME}/Dropbox/Workspace/aliases.sh'
alias lsl='ls -ltrah'
alias psa='ps -eaf'
alias df='df -h'
alias hi='history'
alias emacs='emacs -nw'
alias follow='tail -200f'
alias show='tail -n200'
alias loc='locate'
alias fnohup='follow nohup.out'

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#~~~~~~~~~~~~~~~~~~~~~~
###~~~Development~~~###
#~~~~~~~~~~~~~~~~~~~~~~

###~~~Workspace~~~###
alias setup_python_project="virtualenv venv --python=python3 && source venv/bin/activate && \
pip3 install -U pip-tools oauth2client slackclient pytz && \
pip3 install -e ~/Dropbox/Scripts/python_helper_functions/ && \
ln -s ~/Dropbox/Scripts/python_helper_functions/python_helper_functions . && \
pip-compile --output-file requirements.txt requirements/requirements.in && \
echo -e 'Run *sudo sls plugin install -n serverless-pseudo-parameters* with the params required by your serverless.yml' && \
echo -e 'Run *sudo sls plugin install -n serverless-python-requirements* with the params required by your serverless.yml"

###~~~GIT~~~###

#~~General~~#
alias add='git add .'
alias clone='git clone'
alias pull='git pull'
alias commit='git commit -a -m'
alias push='git push || $(git push 2>&1 >/dev/null | grep git)'
alias fetch='git fetch'
alias fpull='fetch ; pull'

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

###~~~CHEF~~~###

#~~General~~#
alias chefs='chef-solo -c ${HOME}/Dropbox/Scripts/cookbooks/solo.rb -o'

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#~~~~~~~~~~~~~
###~~~S3~~~###
#~~~~~~~~~~~~~

#~~Upload~~#
alias push_ncmms_startup_scripts="aws --profile ncmms s3 sync \
${HOME}/Dropbox/Remote_Mgmt/startup-mgmt/ncmms/ s3://ncmms-instance-mgmt/startup-scripts/"
alias push_ncmms_aliases="aws --profile ncmms s3 cp ${HOME}/Dropbox/Remote_Mgmt/alias-dist/01-ncmms.sh \
s3://ncmms-instance-mgmt/aliases/"

alias push_easi_startup_scripts="aws --profile easi s3 sync \
${HOME}/Dropbox/Remote_Mgmt/startup-mgmt/easi/ s3://easi-instance-mgmt/startup-scripts/"
alias push_easi_aliases="aws --profile easi s3 cp ${HOME}/Dropbox/Remote_Mgmt/alias-dist/01-easi.sh \
s3://easi-instance-mgmt/aliases/"

alias push_internal_appian_startup_scripts="aws --profile internal s3 sync \
${HOME}/Dropbox/Remote_Mgmt/startup-mgmt/internal/ s3://itg-internal-instance-mgmt/startup-scripts/"
alias push_internal_aliases="aws --profile internal s3 cp ${HOME}/Dropbox/Remote_Mgmt/alias-dist/01-internal.sh \
s3://itg-internal-instance-mgmt/aliases/"

alias pushcook_maximo_to_ncmms_bucket="aws --profile ncmms s3 sync ${HOME}/Dropbox/Scripts/cookbooks/s3_transfer/maximo-dir/ \
s3://ncmms-instance-mgmt/cookbooks/maximo-dir/ --exclude '.git*'"
alias pushcook_harden_to_ncmms_bucket="aws --profile ncmms s3 sync ${HOME}/Dropbox/Scripts/cookbooks/s3_transfer/itg-harden-dir/ \
s3://ncmms-instance-mgmt/cookbooks/itg-harden-dir/ --exclude '.git*'"
alias pushcook_javaupdate_to_ncmms_bucket="aws --profile ncmms s3 sync ${HOME}/Dropbox/Scripts/cookbooks/s3_transfer/itg-java-update-dir/ \
s3://ncmms-instance-mgmt/cookbooks/itg-java-update-dir/ --exclude '.git*'"
alias pushcook_symantec_to_ncmms_bucket="aws --profile ncmms s3 sync ${HOME}/Dropbox/Scripts/cookbooks/s3_transfer/symantec-dir/ \
s3://ncmms-instance-mgmt/cookbooks/symantec-dir/ --exclude '.git*'"

alias push_pyhelpers_to_ncmms="aws --profile ncmms s3 sync ${HOME}/Dropbox/Scripts/python_helper_functions \
s3://ncmms-instance-mgmt/helper-functions/python/"

alias upload_aliases="Write a function that uploads this section of your rc to a bucket, \
names it something work-appropriate with a well organized prefix, and eventually make an alias to upload your custom rc"

#~~Pull~~#
alias syncpull_as_ncmms='exportncmms ; aws s3 sync'

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

###~~~Organize Later~~~###

alias resolv='sudo emacs -nw /etc/resolv.conf'
alias np='nohup'
alias saptget='sudo apt-get'
alias sapt='sudo apt'
alias python3='/usr/bin/python3'
alias pmkdir='sudo mkdir'

alias editmyaliases='sudo emacs -nw ${HOME}/Dropbox/Home/aliases.sh'
alias editmyfunctions='sudo emacs -nw ${HOME}/Dropbox/Home/functions.sh'
alias editmyterminal='sudo emacs -nw ${HOME}/Dropbox/Home/startup.sh'
alias edit_my_rc='emacs -nw ${HOME}/.bashrc'
alias edit_my_prof='emacs -nw ${HOME}/.bash_profile'
alias srcal='source ${HOME}/.bashrc'
alias pmv='sudo mv'
alias ptar='sudo tar'
alias ..='cd ..'
alias prm='sudo rm'
alias cdmydownloads='cd ${HOME}/Downloads/'
alias pcp='sudo cp'
alias cdmyprojects='cd ${HOME}/Dropbox/Scripts/projects/'
alias pcho='sudo chown -R'
alias pchm='sudo chmod -R'
alias pupdatedb='sudo updatedb'
alias internalvpn='sudo openvpn --config /etc/openvpn/remote-itg.conf --auth-user-pass ${HOME}/VPN/itg --auth-retry interact'
alias ncmmsvpn='sudo openvpn --config /etc/openvpn/secure-gsa-ncmms.conf --auth-user-pass ${HOME}/VPN/ncmms --auth-retry interact'
alias shinenetvpn='sudo openvpn --config /etc/openvpn/thecloudshine-net.conf --auth-user-pass ${HOME}/VPN/thecloudshinenet --auth-retry interact'
alias internaldns="sudo sed -i '0,/nameserver/ s/nameserver/nameserver 10.5.0.180\nnameserver/' /etc/resolv.conf"
alias ncmmsdns="sudo sed -i '0,/nameserver/ s/nameserver/nameserver 10.178.0.10\nnameserver/' /etc/resolv.conf"
alias shinenetdns="sudo sed -i '0,/nameserver/ s/nameserver/nameserver 10.178.0.10\nnameserver/' /etc/resolv.conf"
alias editdns='sudo emacs -nw /etc/resolv.conf'
alias todo='sudo emacs -nw /etc/issue.net'


alias get_latest_jar=""
alias upload_training_jar="aws --profile core s3 cp ${HOME}/Downloads/$latest_jar s3://itg-internal/Appian/plugins/training/"

alias new_window="gnome-terminal --window"

alias exportdune="export AWS_PROFILE=dune"
alias exportsilverline="export AWS_PROFILE=silverline"
alias exportshine="export AWS_PROFILE=shine"
alias exportcore="export AWS_PROFILE=core"
alias exporteasi="export AWS_PROFILE=easi"
alias exportinternal="export AWS_PROFILE=internal"
alias exportitgplay="export AWS_PROFILE=itg-play"
alias exportncmms="export AWS_PROFILE=ncmms"
alias awsprofiles="cat ${HOME}/.aws/config"

alias view_instance_aliases="cat ${HOME}/scripts/superadmin/00-aliases.sh"
alias edit_instance_aliases="emacs -nw ${HOME}/scripts/superadmin/00-aliases.sh"
alias push_instance_aliases="aws --profile core s3 cp ${HOME}/scripts/superadmin/00-aliases.sh s3://itg-internal/maintenance/"
alias permit_instance_aliases="chmod 644 ${HOME}/scripts/superadmin/00-aliases.sh"

alias view_ncmms_aliases="cat ${HOME}/Dropbox/Remote_Mgmt/alias-dist/01-ncmms.sh"
alias edit_ncmms_aliases="emacs -nw ${HOME}/Dropbox/Remote_Mgmt/alias-dist/01-ncmms.sh"
alias permit_ncmms_aliases="chmod 644 ${HOME}/Dropbox/Remote_Mgmt/alias-dist/01-ncmms.sh"

alias view_easi_aliases="cat ${HOME}/Dropbox/Remote_Mgmt/alias-dist/01-easi.sh"
alias edit_easi_aliases="emacs -nw ${HOME}/Dropbox/Remote_Mgmt/alias-dist/01-easi.sh"
alias permit_easi_aliases="chmod 644 ${HOME}/Dropbox/Remote_Mgmt/alias-dist/01-easi.sh"

#~~~~~~~~~~~~~~
#    Logins
#~~~~~~~~~~~~~~
alias linlogin_easi_both_4_0="printf 'easi\n4.0\nlin\nb' | login"
alias linlogin_easi_both_aries="printf 'easi\naries\nlin\nb' | login"
alias linlogin_easi_both_aquarius="printf 'easi\naquarius\nlin\nb' | login"

alias linlogin_ncmms_all_sbx="printf 'ncmms\nsbx\nlin\nall' | login"
alias linlogin_ncmms_all_dev="printf 'ncmms\ndev\nlin\nall' | login"
alias linlogin_ncmms_all_uat="printf 'ncmms\nuat\nlin\nall' | login"
alias linlogin_ncmms_all_ivv="printf 'ncmms\nivv\nlin\nall' | login"
alias linlogin_ncmms_all_trn="printf 'ncmms\ntrn\nlin\nall' | login"
alias linlogin_ncmms_all_mgmt="printf 'ncmms\nmgmt\nlin\nall' | login"
alias linlogin_ncmms_all_prod="printf 'ncmms\nprod\nlin\nall' | login"

alias linlogin_ncmms_was_sbx="printf 'ncmms\nsbx\nlin\nwas' | login"
alias linlogin_ncmms_was_dev="printf 'ncmms\ndev\nlin\nwas' | login"
alias linlogin_ncmms_was_uat="printf 'ncmms\nuat\nlin\nwas' | login"
alias linlogin_ncmms_was_ivv="printf 'ncmms\nivv\nlin\nwas' | login"
alias linlogin_ncmms_was_trn="printf 'ncmms\ntrn\nlin\nwas' | login"
alias linlogin_ncmms_was_mgmt="printf 'ncmms\nmgmt\nlin\nwas' | login"
alias linlogin_ncmms_was_prod="printf 'ncmms\nprod\nlin\nwas' | login"

alias linlogin_ncmms_db_sbx="printf 'ncmms\nsbx\nlin\ndb' | login"
alias linlogin_ncmms_db_dev="printf 'ncmms\ndev\nlin\ndb' | login"
alias linlogin_ncmms_db_uat="printf 'ncmms\nuat\nlin\ndb' | login"
alias linlogin_ncmms_db_ivv="printf 'ncmms\nivv\nlin\ndb' | login"
alias linlogin_ncmms_db_trn="printf 'ncmms\ntrn\nlin\ndb' | login"
alias linlogin_ncmms_db_mgmt="printf 'ncmms\nmgmt\nlin\ndb' | login"
alias linlogin_ncmms_db_prod="printf 'ncmms\nprod\nlin\ndb' | login"


alias mylogin_ncmms_all_sbx="printf 'ncmms\nsbx\nme\nall' | login"
alias mylogin_ncmms_all_dev="printf 'ncmms\ndev\nme\nall' | login"
alias mylogin_ncmms_all_uat="printf 'ncmms\nuat\nme\nall' | login"
alias mylogin_ncmms_all_ivv="printf 'ncmms\nivv\nme\nall' | login"
alias mylogin_ncmms_all_trn="printf 'ncmms\ntrn\nme\nall' | login"
alias mylogin_ncmms_all_mgmt="printf 'ncmms\nmgmt\nme\nall' | login"
alias mylogin_ncmms_all_prod="printf 'ncmms\nprod\nme\nall' | login"

alias mylogin_ncmms_was_sbx="printf 'ncmms\nsbx\nme\nwas' | login"
alias mylogin_ncmms_was_dev="printf 'ncmms\ndev\nme\nwas' | login"
alias mylogin_ncmms_was_uat="printf 'ncmms\nuat\nme\nwas' | login"
alias mylogin_ncmms_was_ivv="printf 'ncmms\nivv\nme\nwas' | login"
alias mylogin_ncmms_was_trn="printf 'ncmms\ntrn\nme\nwas' | login"
alias mylogin_ncmms_was_mgmt="printf 'ncmms\nmgmt\nme\nwas' | login"
alias mylogin_ncmms_was_prod="printf 'ncmms\nprod\nme\nwas' | login"

alias mylogin_ncmms_db_sbx="printf 'ncmms\nsbx\nme\ndb' | login"
alias mylogin_ncmms_db_dev="printf 'ncmms\ndev\nme\ndb' | login"
alias mylogin_ncmms_db_uat="printf 'ncmms\nuat\nme\ndb' | login"
alias mylogin_ncmms_db_ivv="printf 'ncmms\nivv\nme\ndb' | login"
alias mylogin_ncmms_db_trn="printf 'ncmms\ntrn\nme\ndb' | login"
alias mylogin_ncmms_db_mgmt="printf 'ncmms\nmgmt\nme\ndb' | login"
alias mylogin_ncmms_db_prod="printf 'ncmms\nprod\nme\ndb' | login"

alias linlogin_ncmms_splunk="printf 'ncmms\nmgmt\nlin\nother\nsplunk' | login"

alias 36venv="python3.6 -m venv venv"
alias sovenv="source venv/bin/activate"
