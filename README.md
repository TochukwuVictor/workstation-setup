# Workstation Setup

## Deployment

Add an SSH key to your `.ssh` folder that is capable of pulling (this repository)[git@gitlab.com:TochukwuVictor/workstation-setup.git]. Also modify your `.ssh/config` file so the ssh key added above is used for authenticating with gitlab.com.

The bash deployment script below takes care of bootstrapping your Debian-based workstation.

```bash
./deploy.sh
```
